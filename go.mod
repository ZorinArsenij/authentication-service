module gitlab.com/ZorinArsenij/authentication-service

require (
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/golang/protobuf v1.3.2
	github.com/google/wire v0.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mailru/easyjson v0.7.0
	github.com/onsi/ginkgo v1.10.3 // indirect
	github.com/onsi/gomega v1.7.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/net v0.0.0-20190909003024-a7b16738d86b // indirect
	golang.org/x/sys v0.0.0-20190606165138-5da285871e9c // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.23.1
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)

go 1.13
