// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package entity

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjsonA818f49aDecodeGitlabComZorinArsenijAuthenticationServicePkgAppEntity(in *jlexer.Lexer, out *SessionValue) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "UserID":
			out.UserID = string(in.String())
		case "TokenFCM":
			out.TokenFCM = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonA818f49aEncodeGitlabComZorinArsenijAuthenticationServicePkgAppEntity(out *jwriter.Writer, in SessionValue) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"UserID\":"
		out.RawString(prefix[1:])
		out.String(string(in.UserID))
	}
	{
		const prefix string = ",\"TokenFCM\":"
		out.RawString(prefix)
		out.String(string(in.TokenFCM))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v SessionValue) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonA818f49aEncodeGitlabComZorinArsenijAuthenticationServicePkgAppEntity(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v SessionValue) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonA818f49aEncodeGitlabComZorinArsenijAuthenticationServicePkgAppEntity(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *SessionValue) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonA818f49aDecodeGitlabComZorinArsenijAuthenticationServicePkgAppEntity(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *SessionValue) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonA818f49aDecodeGitlabComZorinArsenijAuthenticationServicePkgAppEntity(l, v)
}
