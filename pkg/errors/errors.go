package errors

import "errors"

var (
	ErrNotFound         = errors.New("not found")
	ErrInconsistentData = errors.New("inconsistent data in session value")
)
