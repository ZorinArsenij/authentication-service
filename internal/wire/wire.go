//+build wireinject

package wire

import (
	"github.com/go-redis/redis"
	"github.com/google/wire"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/app/delivery/grpc/session"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/app/infrastructure/repository"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/app/usecase"
)

//go:generate wire .

func InitializeService(conn *redis.Client) session.SessionService {
	wire.Build(
		session.NewSessionService,
		wire.Bind(new(session.SessionUsecase), new(usecase.SessionUsecase)),
		usecase.NewSessionUsecase,
		wire.Bind(new(usecase.SessionRepository), new(repository.Repository)),
		repository.New)
	return session.SessionService{}
}
