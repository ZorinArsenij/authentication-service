package redis

import "github.com/go-redis/redis"

// NewConnection create new connection with redis
func NewConnection(addr, password string) (*redis.Client, error) {
	opts := &redis.Options{
		Addr:     addr,
		Password: password,
	}

	conn := redis.NewClient(opts)
	if _, err := conn.Ping().Result(); err != nil {
		return nil, err
	}

	return conn, nil
}
